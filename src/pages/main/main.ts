import { AuthService } from './../../providers/auth';
import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-main',
  templateUrl: 'main.html'
})
export class MainPage {

  constructor(public navCtrl: NavController,
                public menuCtrl: MenuController,
                public authService: AuthService) {
    
  }
}
  