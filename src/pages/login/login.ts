import { MainPage } from './../main/main';
import { Component } from '@angular/core';
import { NavController, AlertController, Platform, MenuController  } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { AuthService } from "../../providers/auth";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  
  //auth: any;
  //authType: string = "login";
  //contentHeader: any;
  //jwtHelper: any;
  //local: any;
  //user: string;

  //nuevas variables
  public form:FormGroup;
  loading: boolean;
  error: boolean;
  errorMessage: string;
  public usuario: AbstractControl;
  public clave: AbstractControl;

  constructor(public platform: Platform,
    public loadingCtrl: LoadingController, 
    public navCtrl: NavController, 
    private authService: AuthService, 
    fb:FormBuilder, 
    public alertCtrl: AlertController,
    public menuCtrl: MenuController) {

      this.menuCtrl.enable(false, 'menuPrincipal');

      this.loading = false;
      this.error = false;
      this.errorMessage = "";

      this.form = fb.group({
        'usuario': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
        'clave': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
      });

      this.usuario = this.form.controls['usuario'];
      this.clave = this.form.controls['clave'];
      this.form.controls['usuario'].setValue("");
      this.form.controls['clave'].setValue("");
      this.usuario.setValue("");
      this.clave.setValue("");
      this.form.reset();

      this.platform.ready().then(() => {
        //this.authService.logout();
      });
  }
  
  public onSubmit(value:any):void {
      let loader = this.loadingCtrl.create({
        content: "Aguarde por favor..."
      });
      loader.present();
      console.log("se ejecuto el submit")
      this.loading = true;
      this.error = false;
      this.authService.login(value.usuario, value.clave)
         .subscribe(
            data => {
              if(data){
                this.loading = false;
                //COnsulto y sete en una variable local los cuestionarios para el usuario
                //this.controlesInicioManagerService.setCuestionarios();
                loader.dismiss();
                this.menuCtrl.enable(true, 'menuPrincipal');
                this.navCtrl.setRoot(MainPage);
                //this.navCtrl.push(MainPage);
              }
              else{
                this.error = true;
                //console.log("Errort: " + errorResult);
                //console.log("Error1: " + JSON.parse(errorResult).ExceptionMessage);
                this.errorMessage = this.authService.mensajeRespuesta;
                this.loading = false;
                loader.dismiss();
                let alert = this.alertCtrl.create({
                  title:'Error en login', 
                  subTitle:this.authService.mensajeRespuesta,
                  buttons:['OK']
                });
                alert.present();  
              }
            },  
            errorResult => {
              this.error = true;
              console.log("Errort: " + errorResult);
              console.log("Error1: " + JSON.parse(errorResult).ExceptionMessage);
              this.errorMessage = (JSON.parse(errorResult).ExceptionMessage || "Hemos detectado un inconveniente. Intente nuevamente");
              this.loading = false;
              loader.dismiss();
            }
         );
  }

}
