import { _MensajeRespuesta } from './../models/_mensajeRespuesta';
import { FactoryLlamadaService } from './factoryLlamada';
import { Usuario } from './../models/usuario';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { ConstApiUrl } from "../models/constApiUrl";
import { Observable }     from 'rxjs/Observable'; 

import 'rxjs/add/operator/map'; 
import 'rxjs/add/operator/catch'
import 'rxjs/add/observable/throw'
import 'rxjs/Rx'

import { ContextoSeguridad } from "../models/contextoSeguridad";

@Injectable()
export class AuthService {
  
  public isLoggedIn: boolean = false;
  public contextoSeguridad: ContextoSeguridad;
  
  apiUrl = ConstApiUrl.LOGIN;
  mensajeRespuesta: string;

  constructor(public http: Http, public fLlService: FactoryLlamadaService) {
    console.log('+ AuthService > constructor');
    this.isLoggedIn = false;
    this.http = http;
  }

  login (p_usuario: string, p_clave: string){
    console.log('+ AuthService > login');
    //////////////////////////////////////////////////////////////////////////////////////////////////

    var options = new RequestOptions({headers: new Headers({
      'Content-Type': 'application/json'
    })});

    var stringPost: string;

    var user: Usuario;
    user = new Usuario();
    
    user.idUsuario= 0;
    user.nombreusuario = p_usuario;
    user.clave = p_clave;
    user.reestablece_clave= false;
    user.bloqueado= false;
    user.intentos_incorrectos= null;
    user.clave_anterior= null;

    //le asigno al objeto llamada el objeto usuario:
    console.log("Usuario: " + user);
    stringPost = JSON.stringify(this.fLlService.createMensajeLlamada(user));
    console.log("stringPost: " + stringPost);

    return this.http
      .post(this.apiUrl,stringPost,options)
      .map((result)=>{

        console.log("Codigo respuesta Login: " + (<_MensajeRespuesta>result.json()).header.codigoRespuesta);
        if ((<_MensajeRespuesta>result.json()).header.codigoRespuesta==0){
          this.obtenerDatos(result);
          return true;
        }
        else{
          console.log("No es respuesta == 0");
          console.log("mensaje de respuesta del login: " + (<_MensajeRespuesta>result.json()).header.mensajeTecnico);
          this.mensajeRespuesta = (<_MensajeRespuesta>result.json()).header.mensajeTecnico;
          return false;
        }
      })
      /*
      .map(this.obtenerDatos)
      .map(()=>{
        this.isLoggedIn = true;
        return true;
      })
      */
      .catch(this.tratarErrores);
      //////////////////////////////////////////////////////////////////////////////////////////////////
    
  }

  obtenerDatos(r) {
    var respuesta = new _MensajeRespuesta();
    this.contextoSeguridad = new ContextoSeguridad();
    this.contextoSeguridad.usuarioAutenticado = true
    respuesta = r.json();
    this.contextoSeguridad.usuario = respuesta.data;

    //localStorage.setItem(ConstDataLocalStorage.CONTEXTO_SEGURIDAD, JSON.stringify(contexoSeguridad));
    console.log(this.contextoSeguridad);
  }

  // tratar errores de comunicación
  tratarErrores(error) {
    //console.log("tratarErrores");
    //console.log(JSON.stringify(error));
    /*
    if (error.status == 401) {console.log("Error de permisos");}
    else {//}
    */
    this.isLoggedIn = false;
    return Observable.throw(error._body)
  } 
}
