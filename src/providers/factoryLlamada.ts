import { Injectable } from '@angular/core';
import { _MensajeLlamada } from '../models/_mensajeLlamada';
import { _HeaderLlamada } from '../models/_headerLlamada';
import { ConstDataHeader } from "../models/constDataHeader";

@Injectable()
export class FactoryLlamadaService {

  public createMensajeLlamada(data:any):_MensajeLlamada{
    var mll = new _MensajeLlamada();
    mll.header = new _HeaderLlamada();
    mll.header.datosDispositivo = "datosDispositivos";
    mll.header.apiVersion = ConstDataHeader.VERSION_API;
    mll.header.appVersion = ConstDataHeader.VERSION_APP;
    mll.header.fechahora = new Date();
    mll.header.geolocalizacion = "x";
    mll.header.idDispositivo = "aabb1122";
    mll.header.token = "aabb1122";
    mll.header.idUsuario = 0;
    mll.data = data;
    return mll;
  }


}
