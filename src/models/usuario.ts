import { Perfil } from './perfil';
 
  export class Usuario {
    idUsuario: number;
        idPerfil: number;
        nombreusuario: string;
        clave: string;
        reestablece_clave: boolean;
        bloqueado: boolean;
        intentos_incorrectos: number;
        foto_perfil: string;
        nombre: string;
        apellido: string;
        email: string;
        clave_anterior: string;
        perfil: Perfil;
        token: string;
  }
