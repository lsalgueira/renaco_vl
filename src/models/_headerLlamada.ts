
export class _HeaderLlamada {
    public fechahora: Date;
    public geolocalizacion: string;
    public idDispositivo:string;
    public datosDispositivo: string;
    public token: string;
    public idUsuario: number;
    public appVersion: string;
    public apiVersion: string;
}
