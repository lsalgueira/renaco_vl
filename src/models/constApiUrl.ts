export class ConstApiUrl {
    public static readonly CONTROL_ALTA = "http://services.groobits.com/api/controls/postcontrol";
    public static readonly ENTRADAS_POR_CONTROL = "http://services.groobits.com/api/Entradas/postgetentradabycontrol";
    public static readonly SINCRONIZAR_CONTROLES = "http://services.groobits.com/api/ControlRealizados/PostControlRealizado";  
    public static readonly LOGIN = "http://services.groobits.com/api/usuarios/postlogin";   
}