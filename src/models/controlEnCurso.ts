import { Control } from './control';
import { ControlRealizado } from './controlRealizado';

export class ControlEnCurso {
  Estado: string;
  UltimaPregunta: number;
  CuestionarioIn: Control;
  CuestionarioOut: ControlRealizado;
}
