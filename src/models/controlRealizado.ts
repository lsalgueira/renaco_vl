import { Control } from './control';
import { EntradaRealizada } from './entradaRealizada';

  export class ControlRealizado {
		control: Control;
		controlRealizadoPosiciones: any;
		entradasRealizadas: EntradaRealizada[];
		fechahora_creacion: Date;
		fechahora_fin: Date;
		fechahora_inicio: Date;
		fechahora_modif: Date;
		fechahora_sup: Date;
		fechas_posiciones: string;
		flag_sup: boolean;
		idControl: number;
		idControlRealizado: number;
		idMobile: number;
		idUsuario_control: number;
		idUsuario_creacion: number;
		idUsuario_modif: number;
		idUsuario_sup: number;
		posiciones: string[];
		idEstado: number;
	}



