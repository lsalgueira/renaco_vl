import { Entrada } from './entrada';

export class EntradaRealizada {
		binarioRespuesta: string;
		controlRealizado: any;
		entrada: Entrada;
		fechahora_creacion: Date;
		fechahora_modif: Date;
		fechahora_sup: Date;
		flag_sup: boolean;
		idControlRealizado: number;
		idEntrada: number;
		idEntradaRealizada: number;
		idUsuario_creacion: number;
		idUsuario_entrada: number;
		idUsuario_modif: number;
		idUsuario_sup: number;
		latitud: string;
		longitud: string;
		orden: number;
		textoRespuesta: string;
	}