import { Permiso } from "./permiso";
  
  export class Perfil {
        idPerfil: number;
        nombre: string;
        descripcion: string;
        permisos: Permiso[];
  }
