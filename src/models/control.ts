import { Entrada } from './entrada';

export class Control {
		descripcion: string;
		entradas: Entrada[];
		fechahora_creacion: Date;
		fechahora_modif: Date;
		fechahora_sup: Date;
		flag_sup: boolean;
		icono: string;
		idControl: number;
		idUsuario_creacion: number;
		idUsuario_modif: number;
		idUsuario_sup: number;
		nombre: string;
	}