export class _HeaderRespuesta {
	codigoRespuesta: number;
	mensajeUsuario: string;
	mensajeTecnico: string;
	codigoRespuestaServicio: string;
}