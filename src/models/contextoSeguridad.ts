import { Usuario } from './usuario';

export class ContextoSeguridad {
  usuario: Usuario;
  usuarioAutenticado: boolean;
}
