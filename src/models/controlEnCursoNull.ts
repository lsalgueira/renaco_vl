import { ControlEnCurso } from './controlEnCurso';
export class ControlEnCursoNull {
    public static readonly CONTROL:ControlEnCurso = {
        Estado: "inicio",
        UltimaPregunta: 1,
        CuestionarioIn: null,
        CuestionarioOut: null
    };
}