import { Control } from './control';
export class Entrada {
		control: Control;
		fechahora_creacion: Date;
		fechahora_modif: Date;
		fechahora_sup: Date;
		flag_sup: boolean;
		idControl: number;
		idEntrada: number;
		idTipoEntrada: number;
		idUsuario_creacion: number;
		idUsuario_modif: number;
		idUsuario_sup: number;
		orden: number;
		textoAyuda: string;
		textoConfirmacionNegativa: string;
		textoEntrada: string;
		textoEntradaReducido: string;
		textoNegativa: string;
		tipoEntrada: any;
	}